#include <iostream>
//#include <stdlib.h>
#include <stdio.h>
//#include <highgui.h>
#include <opencv2/opencv.hpp>
#include "Objeto.h"


#define PIXEL_MIN		500//quantidade minima para que considere um obj
#define REGIAO 			45//tamanho da regiao que vai de cada lado, max 50

using namespace std;
using namespace cv;


//-----------------funcao que desenha duas linhas na imagem ---------------------
void desenhaReg(Mat* f){
	line(*f,Point(f->cols*REGIAO/100,0),Point(f->cols*REGIAO/100,f->cols-1),Scalar(0,255,255),0.1);
	line(*f,Point(f->cols*(1-REGIAO/100.0),0),Point(f->cols*(1-REGIAO/100.0),f->cols-1),Scalar(0,255,255),0.1);
}

//funçao que encontra todos os pixels de um objeto
long int findObj(Mat* m, Mat* f, int i, int j, Objeto* Obj, int *dados){
	long int soma=0;
	if(i<f->rows&&j<f->cols){
			if(Obj->verificaH(m->at<Vec3b>(i,j)[0]) && Obj->verificaS(m->at<Vec3b>(i,j)[1]) && Obj->verificaV(m->at<Vec3b>(i,j)[2])){
				soma++;
				Obj->addPoint(i,j);
				int cor = m->at<Vec3b>(i,j)[0];
				m->at<Vec3b>(i,j)[0] = 255;
				m->at<Vec3b>(i,j)[1] = 0;
				m->at<Vec3b>(i,j)[2] = 0;

				f->at<Vec3b>(i,j)[0] = 255;
				f->at<Vec3b>(i,j)[1] = 255;
				f->at<Vec3b>(i,j)[2] = 0;

				soma += findObj(m,f,i+1,j,Obj, dados);
				soma += findObj(m,f,i-1,j,Obj, dados);
				soma += findObj(m,f,i,j-1,Obj, dados);
				soma += findObj(m,f,i,j+1,Obj, dados);

				dados[soma]=cor;
			}
	}
	return soma;
}

bool testeEstatistico(int *dados, int quantidadePixel){
	float Zcritico = 1.96;
	double Soma = 0;
	for (int i = 0; i<quantidadePixel ;i++){
		Soma += dados[i];
	}
	double media = Soma/quantidadePixel;
	Soma = 0;
	for (int i = 0; i<quantidadePixel ;i++){
		Soma += pow((dados[i]-media),2);
	}
	double devpad = sqrt((1.0/quantidadePixel)*Soma);
	double stat_teste = (media-106)/(2.1385/sqrt(quantidadePixel)); 

	if((stat_teste)>(-Zcritico)&&(stat_teste<Zcritico)){
		return true;
	} else{
		return false;
	}

}
//funçao que indifica objettos na imagem, objeto sera indeticado com base nas caracteristicas de obj
void indentifica(Objeto* obj, Mat* f, Mat* h, int *dados){
	for (int i = 0; i < f->rows; i++){
		for (int j = 0; j < f->cols; j++){	
			long int quantidadePixel = findObj(h,f,i,j,obj->getUltimo(),dados);
			if(quantidadePixel > PIXEL_MIN ){//&& testeEstatistico(dados, quantidadePixel)){//se encontrar um objrto com mais de 1000 pixels
				obj->add();	
			}else{
				obj->getUltimo()->resetObj();//reinicia o objeto caso nao encontre nada 
			}

		}
	}
			
}



int main(){

	VideoCapture cap; // open the default camera
	Mat frame;
	Mat hsv;


	cap = VideoCapture(0);

	if(!cap.isOpened())
		return -1;
	cap.set(CV_CAP_PROP_FRAME_WIDTH, 352);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 288);
	cout << "Running..." << endl;

	cap >> frame;
	int quantidadePixel = frame.rows*frame.cols;

	namedWindow("Video",1);

	for(;;){
		//coloca um frame de VideoCapture na variavel frame
		cap >> frame;
		
		// cria uma imagem em hsv a partir de frame
		cvtColor(frame,hsv,CV_BGR2HSV,0);
		int quantidadePixel = frame.rows*frame.cols;


		int dados [quantidadePixel];


		//cria um objeto de cor azul, este objeto é na verdade uma lista de si mesmo
		Objeto objA;
		objA.inicializa(76, 62, 87, 124,221,158,0, 0, 255,&objA);	//hue hueRange satMin valMin B G R -- inicializa o objeto com as cores a se procurar

		indentifica(&objA,&frame,&hsv,dados);// procura os pixels da cor definida no obj
		//------------------------------------------------------------------------------
		//------indentifica o objeto com mais pixels------------------------------------
		int idMaior = 0;
		for(int i =0;i<objA.getContador();i++){
			if(objA.getById(i)->getQuantidadeP()>objA.getById(idMaior)->getQuantidadeP())
				idMaior=i;
		}
		//-----------------------------------------------------------------------------
		//------marca os centroids dos objetos indetificados, o que tem mais pixels sera o de cor diferente
		for(int i =0;i<objA.getContador();i++){
			Scalar cor(255,255,255);
			if(i==idMaior)	
				cor = Scalar(0,0,255);
			line(frame,objA.getById(i)->getCentroid(),objA.getById(i)->getCentroid(),cor,10);//centroide do objeto 1
		}
		//------------------------------------------------------------------------------
			if(objA.getContador()!=0){
				//para saber para onde se deve girar o robo
				if(objA.getById(idMaior)->getCentroid().x>frame.cols*(1-REGIAO/100.0))
					cout<<"direita "<<objA.getById(idMaior)->getCentroid().x-frame.cols*(1-REGIAO/100.0)<<" pixels"<<endl;
				else if(objA.getById(idMaior)->getCentroid().x<frame.cols*REGIAO/100)
					cout<<"esquerda "<<frame.cols*(REGIAO/100.0)-objA.getById(idMaior)->getCentroid().x<<" pixels"<<endl;
				else
					cout<<"ir para frente?"<<endl;
			}else {
				system("clear");
				cout<<"fora de alcance"<<endl;
			}
		
		
		
		desenhaReg(&frame);//desenha no frame as linhas de define a regiao de interece
		imshow("Video", frame);
			objA.resetAll();//reinicia o objeto, "apaga a lista"
//----------para o for infinito----------------
		if(cvWaitKey(1) == 27)
			break;
	}
	cap.release();
	frame.release();
	hsv.release();

	return 0;
}