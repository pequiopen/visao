#include "Objeto.h"
#include <stdio.h>
#include <iostream>
//#include <stdlib.h>
#include <stdio.h>
#define HUE_MAX			180

Objeto::Objeto(){ }

void Objeto::inicializa(int indentidade, int hA, int hB, int sA, int sB, int vA, int vB, int b,int r,int g){ // Blue, Red, Green
	quantidadePontos = 0;
	somaX = 0;
	somaY = 0;
	contador = 0;

	id = indentidade;

	hueMax = std::max(hA,hB);
	hueMin = std::min(hA,hB);		
	satMax = std::max(sA,sB);
	satMin = std::min(sA,sB);
	valMax = std::max(vA,vB);
	valMin = std::min(vA,vB);
	
	B = b;
	R = r;
	G =	g;

	PonteiroProx =	NULL;
	PonteiroThis =	NULL;
	PonteiroAnte =	NULL;
}

void Objeto::inicializa(int hA, int hB, int sA, int sB, int vA, int vB, int b,int r,int g , Objeto* pntProp){ // Blue, Red, Green
	quantidadePontos = 0;
	somaX = 0;
	somaY = 0;
	
	contador = 0;

	id = 0;

	
	hueMax = std::max(hA,hB);
	hueMin = std::min(hA,hB);		
	satMax = std::max(sA,sB);
	satMin = std::min(sA,sB);
	valMax = std::max(vA,vB);
	valMin = std::min(vA,vB);
	
	B = b;
	R = r;
	G =	g;


	PonteiroProx =	NULL;
	PonteiroThis =	pntProp;
	PonteiroAnte =	pntProp;
}

cv::Point Objeto::getCentroid(){
	cv::Point x(1,0);
	cv::Point y(0,1);
	centroid = 0*x+0*y;

	if(quantidadePontos!=0){
		centroidX = somaX/quantidadePontos;
		centroidY = somaY/quantidadePontos;
		centroid = centroidX*x+centroidY*y;
	}

	return centroid;

}

long int Objeto::getQuantidadeP(){
	return quantidadePontos;
}

int Objeto::getContador(){
	return contador;
}

bool Objeto::verificaS(int x){
	if(x>=satMin && x<=satMax){
		return true;
	}else{
		return false;
	}
}

bool Objeto::verificaV(int x){
	if(x>=valMin && x<=valMax){
		return true;
	}else{
		return false;
	}
}

bool Objeto::verificaH(int x){
	if((hueMax - hueMin) < HUE_MAX*0.75){
		if(x>=hueMin && x<=hueMax){
			return true;
		}else{
			return false;
		}
	} else {
		if(x<=hueMin || x>=hueMax){
			return true;
		}else{
			return false;
		}
	}
}


void Objeto::addPoint(int i, int j){
	somaX += j;
	somaY += i;
	quantidadePontos ++;
}

void Objeto::resetObj(){
	quantidadePontos = 0;
	somaX = 0;
	somaY = 0;
}


Objeto* Objeto::getUltimo(){
	if(PonteiroProx==NULL){
		return PonteiroThis;
	}else{
		return PonteiroProx->getUltimo();
	}
}


void Objeto::setPonteiro(Objeto* Anterior, Objeto* Proprio){
	PonteiroThis =	Proprio;
	PonteiroAnte =	Anterior;
}

void Objeto::setPonteiro(Objeto* Proprio){
	PonteiroThis =	Proprio;

}

void Objeto::setToUltimo(){
	PonteiroProx = NULL;
}

Objeto* Objeto::getAnterior(){
	return PonteiroAnte;
}

Objeto* Objeto::getProximo(){
	return PonteiroProx;
}

Objeto* Objeto::getThis(){
	return PonteiroThis;
}

Objeto* Objeto::getById(int i){
	if(id==i||PonteiroProx==NULL)
		return PonteiroThis;
	else
		return PonteiroProx->getById(i);
}

void Objeto::resetAll(){
	contador = 0;
	while(PonteiroProx!=NULL){
		Objeto* aux = PonteiroThis->getUltimo();
		PonteiroThis->getUltimo()->getAnterior()->setToUltimo();
		free(aux);
	}
}

void Objeto::add(){
	contador++;
	if(PonteiroProx==NULL){	
		PonteiroProx = (Objeto*)malloc(sizeof(Objeto));
		PonteiroProx->inicializa(id+1, hueMax, hueMin, satMax, satMin, valMax, valMin, B, R, G);
		PonteiroProx->setPonteiro(PonteiroThis,PonteiroProx);

	}else{
		PonteiroProx->add();
	}	
}
