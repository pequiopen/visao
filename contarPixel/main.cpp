#include <iostream>
//#include <stdlib.h>
#include <stdio.h>
//#include <highgui.h>
#include <opencv2/opencv.hpp>
#include "Objeto.h"
#include <math.h>

#define PIXEL_MIN		200//quantidade minima para que considere um obj
#define VIDEO_LARGURA 	352
#define VIDEO_ALTURA 	288

using namespace std;
using namespace cv;


//funcao que expande as bordas encontrada pela canny
void expandeBorda(Mat* f, int pi, int pj,int espessura){
	for(int i= pi-espessura; i<=pi+espessura;i++){
		for(int j=pj-espessura; j<=pj+espessura;j++){
			if(i>0 && i<f->rows && j>0 && j<f->cols){
				f->at<Vec3b>(i,j)[0] = 0;
				f->at<Vec3b>(i,j)[1] = 0;
				f->at<Vec3b>(i,j)[2] = 0;
			}
		}
	}	 				
}

//funçao que encontra todos os pixels de um objeto
long int findObj(Mat* m, Mat* f, int i, int j, Objeto* Obj){
	long int soma=0;
	if(i<f->rows&&j<f->cols){
			//verificar se o pixel é da cor desejada
			if(Obj->verificaH(m->at<Vec3b>(i,j)[0]) && Obj->verificaS(m->at<Vec3b>(i,j)[1]) && Obj->verificaV(m->at<Vec3b>(i,j)[2])){
				soma++;
				Obj->addPoint(i,j);
				int cor = m->at<Vec3b>(i,j)[0];
				//apagar o pixel ja encontrado
				m->at<Vec3b>(i,j)[0] = 255;
				m->at<Vec3b>(i,j)[1] = 0;
				m->at<Vec3b>(i,j)[2] = 0;
				//marcar no frame de saida o pixel encontrado
				f->at<Vec3b>(i,j)[0] = 255;
				f->at<Vec3b>(i,j)[1] = 255;
				f->at<Vec3b>(i,j)[2] = 0;
				//procurar por pixels ao redor deste
				soma += findObj(m,f,i+1,j,Obj);
				soma += findObj(m,f,i-1,j,Obj);
				soma += findObj(m,f,i,j-1,Obj);
				soma += findObj(m,f,i,j+1,Obj);
			}
	}
	return soma;
}

//funçao que indifica objettos na imagem, objeto sera indeticado com base nas caracteristicas de obj
void indentifica(Objeto* obj, Mat* f, Mat* h){
	for (int i = 0; i < f->rows; i++){
		for (int j = 0; j < f->cols; j++){	
			long int quantidadePixel = findObj(h,f,i,j,obj->getUltimo());
			if(quantidadePixel > PIXEL_MIN ){//se encontrar um objrto com mais de PIXEL_MIN pixels
				obj->add();	
			}else{
				obj->getUltimo()->resetObj();//reinicia o objeto caso nao encontre nada 
			}

		}
	}
			
}



int main(){

	VideoCapture cap;
	Mat frame;
	Mat hsv;
	Mat gray;
	Mat plano;
	Mat planoSaida;
	int exibirOpcao = 0;// 0 mostra a quantidade de pixel, 1 mostra a posicao Cm, 2 mostra posicao em pixels

	cap = VideoCapture(0);

	if(!cap.isOpened())
		return -1;
	//define o tamanho da imagem a ser capturada
	cap.set(CV_CAP_PROP_FRAME_WIDTH, VIDEO_LARGURA);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, VIDEO_ALTURA);
	cvWaitKey(100);//esperar a camera inicializar
	cout << "Running..." << endl;

	//abre uma janela para mostrar o frame
	namedWindow("Video",1);
	//abre uma janela para mostrar a pos do obj
	namedWindow("PlanoXY",1);
	//desenhar o plano xy--------------------------------------------------------------------
	cap >> frame;
	cvtColor(frame,plano,CV_BGR2GRAY,0);
	for(int i = 0; i<plano.rows; i++){
		for(int j = 0; j<plano.cols; j++){
			plano.at<uchar>(i,j) = 0;//pinta tudo de preto
		}
	}	
	for(int i = 1; i<=10;i++){
		line(plano,Point(i*plano.cols/10,0),Point(i*plano.cols/10,plano.rows),60,1);//verticais
		line(plano,Point(0,i*plano.rows/10),Point(plano.cols,i*plano.rows/10),60,1);//horizontais
		putText(plano,to_string(i), Point(plano.cols/2,plano.rows-i*plano.rows/10), CV_FONT_HERSHEY_COMPLEX, 0.5, 255, 1 ,8);
		putText(plano,to_string(i), Point(i*plano.cols/10+plano.cols/2,plano.rows-5), CV_FONT_HERSHEY_COMPLEX, 0.5, 255, 1 ,8);
		putText(plano,to_string(-i), Point(plano.cols/2-i*plano.cols/10,plano.rows-5), CV_FONT_HERSHEY_COMPLEX, 0.5, 255, 1 ,8);
	}
	line(plano,Point(plano.cols/2,0),Point(plano.cols/2,plano.rows),255,2);//eixo vetical
	line(plano,Point(0,plano.rows-1),Point(plano.cols,plano.rows-1),255,2);//eixo horizontal
	line(plano,Point(plano.cols/2,plano.rows),Point(plano.cols/2-plano.cols*0.466308,0),255,1);//linha de campo de visao
	line(plano,Point(plano.cols/2,plano.rows),Point(plano.cols/2+plano.cols*0.466308,0),255,1);//linha de campo de visao

	//cria uma categoria de objeto de cor azul
	Objeto objA;
	//-------------(hA, hB, sA, sB, vA, vB, b, r, g , pntProp)
	objA.inicializa(111, 106, 255, 196,137,188 ,0, 0, 255,  &objA);	

	for(;;){
		planoSaida = plano.clone();
		//coloca um frame de VideoCapture na variavel frame
		cap >> frame;
		//blur( frame, frame, Size(2,2)); //se quiser diminuir a quandidade de ruido da imagem 		
		cvtColor(frame,gray,CV_BGR2GRAY,0);
		cvtColor(frame,hsv,CV_BGR2HSV,0);

		blur( gray, gray, Size(3,3));

		Canny(gray, gray, 0,5 , 3);

 		for(int i = 0; i<frame.rows; i++){
			for(int j = 0; j<frame.cols; j++){
 				if(gray.at<uchar>(i,j) == 255){
 					expandeBorda(&frame,i,j,1);
 				}
 			} 			
 		}

		indentifica(&objA,&frame,&hsv);// procura os pixels da cor definida no objA
		//------------------------------------------------------------------------------
		//------indentifica o objeto com mais pixels------------------------------------
		int idMaior = 0;
		for(int i =0;i<objA.getContador();i++){
			if(objA.getById(i)->getQuantidadeP()>objA.getById(idMaior)->getQuantidadeP())
				idMaior=i;
		}
		//-----------------------------------------------------------------------------
		//------marca os centroids dos objetos indetificados, o que tem mais pixels sera o de cor diferente
		for(int i =0;i<objA.getContador();i++){
			Scalar cor(255,255,255);
			if(i==idMaior){
				cor = Scalar(0,0,255); 			
			}
			line(frame,objA.getById(i)->getCentroid(),objA.getById(i)->getCentroid(),cor,5);//marca um ponto no centroid de cada obj

			if(exibirOpcao == 0){//mostrar quantidade de pixxels
				putText(frame,to_string(objA.getById(i)->getQuantidadeP()), objA.getById(i)->getCentroid(), CV_FONT_HERSHEY_COMPLEX, 0.5, Scalar(0,0,0),2 ,8);
				putText(frame,to_string(objA.getById(i)->getQuantidadeP()), objA.getById(i)->getCentroid(), CV_FONT_HERSHEY_COMPLEX, 0.5, cor,1 ,8);	
			}else if(exibirOpcao == 1){//mostrar a posicao em cood. polar
				//double area = 3.36*9; //centimetros quadrado
				double area = 6.9;
				double modulo = 327.32572*pow(objA.getById(i)->getQuantidadeP()/area,-0.4810555);
				double angulo = (25.0/175.0)*(objA.getById(i)->getCentroid().x - frame.cols/2.0);

				int posx = modulo*sin(angulo*(3.14159/180));
				int posy = modulo*cos(angulo*(3.14159/180));

				posx = posx*frame.cols/100;
				posy = posy*frame.rows/100;
				posx = posx + frame.cols/2;
				posy = frame.rows - posy;
				
				line(planoSaida,Point(posx,posy),Point(posx,posy),255,10);
				//stringstream centroideStr;
				//centroideStr.precision(3);
				//centroideStr <<"("<<modulo<<"<"<<angulo<<")";
				//putText(frame,centroideStr.str(), objA.getById(i)->getCentroid(), CV_FONT_HERSHEY_COMPLEX, 0.5, Scalar(0,0,0),2 ,8);
				//putText(frame,centroideStr.str(), objA.getById(i)->getCentroid(), CV_FONT_HERSHEY_COMPLEX, 0.5, cor,1 ,8);
			}else if(exibirOpcao == 2){//mostrar posicao em pixels x,y
				//pega o centroid x,y de cada obj
				int posx = objA.getById(i)->getCentroid().x;
				int posy = objA.getById(i)->getCentroid().y;
				//muda a origem para o centro inferior da imagem
				posx = posx - frame.cols/2;
				posy = frame.rows - posy;
				//coloca os valores em texto
				stringstream centroideStr;
				centroideStr.precision(3);
				centroideStr <<"("<<posx<<","<<posy<<")";
				putText(frame,centroideStr.str(), objA.getById(i)->getCentroid(), CV_FONT_HERSHEY_COMPLEX, 0.5, Scalar(0,0,0),2 ,8);
				putText(frame,centroideStr.str(), objA.getById(i)->getCentroid(), CV_FONT_HERSHEY_COMPLEX, 0.5, cor,1 ,8);
			}
		}
		//------------------------------------------------------------------------------
		
		//Imprime a imagem
		imshow("Video", frame);
		//imprime o plano
		imshow("PlanoXY", planoSaida);
		objA.resetAll();//reinicia o objeto, "apaga a lista"

		//==Botoes de controle=====================================
		int a = cvWaitKey(1);//pega uma tecla pressionada
		
		if(a == 27){//se for o ESC sai do progama-------------------------------------------------------------------------------------
			break;
		}else if(a == 102){//se for f muda qual sera a informaçao acerca do obj que sera poltada--------------------------------------
			exibirOpcao++;
			if(exibirOpcao>2)
				exibirOpcao = 0;
		}else if(a == 108){//se for a letra l, coloca linha paraleas na tela, bom pra calibrar----------------------------------------
			int quantidadeLinhas = 6;
			for(;;){
				cap >> frame;
				int delta = frame.cols/quantidadeLinhas;
				for(int i = 0; i<=quantidadeLinhas;i++){
					line(frame,Point((i*delta),0),Point((i*delta),frame.cols),Scalar(0,255,255),1);
				}
				imshow("Video", frame);
				int a =	cvWaitKey(1);
				if(a==108)//espera apertar l pra voltar para o loop principal
					break;
			}
		}else if(a == 99){//se letra c, recalibrar os parametros HSV------------------------------------------------------------------
			int xreg = 0;//posicao x do centro do quadrado em que estao os dados 
			int yreg = 0;//posicao y do centro do quadrado em que estao os dados
			int DIM = 10;//dimensao em pixels do quadrado
			int Zc = 200;//elemento que define o intevalo a ser adotado, quanto maior maior o intervalo
			int unidadeDIM = 10;//aumentar ou diminuir a regiao em mutiplos deste valor
			int unidadeREG = 5;//aumentar ou diminuir a pos x,y em mutiplos deste valor

			for(;;){
				cap >> frame;
				cvtColor(frame,hsv,CV_BGR2HSV,0);
				int quantidadePixel = frame.rows*frame.cols;
				
				int dados [quantidadePixel][3];

				int contador_dados = 0; 
				for (int i = (frame.rows-DIM)/2+yreg; i < (frame.rows+DIM)/2+yreg; i++){
					for (int j = (frame.cols-DIM)/2+xreg; j < (frame.cols+DIM)/2+xreg; j++){
						if(i>0&&i<frame.cols&&j>0&&j<frame.rows){//para nao olhar fora da matriz
							//adiciona o pixel a lista de dados
							dados[contador_dados][0]=hsv.at<Vec3b>(i,j)[0];
							dados[contador_dados][1]=hsv.at<Vec3b>(i,j)[1];
							dados[contador_dados][2]=hsv.at<Vec3b>(i,j)[2];
							//avisa que aumentou mais um pixel
							contador_dados++;
							//pinta a imagem de saida para sabermos onde fica a regiao
							frame.at<Vec3b>(i,j)[0] = 0;
							frame.at<Vec3b>(i,j)[1] = 0;	
							frame.at<Vec3b>(i,j)[2] = 0;
						}
					}
				}

				double Soma[3];
				double media[3];
				double devpad[3];		
				int MediaLim[3][2];
				for(int k = 0 ; k<3; k++){//faz o calculo para os tres elementos HSV
					//calcula a media..................................
					Soma[k] = 0;
					for (int i = 0; i<contador_dados ;i++){
						Soma[k] += dados[i][k];
					}
					media[k] = Soma[k]/contador_dados;
					//calcula o devpad.................................
					Soma[k] = 0;			
					for (int i = 0; i<contador_dados ;i++){
						Soma[k] += pow((dados[i][k]-media[k]),2);
					}
					devpad[k] = sqrt((1.0/contador_dados)*Soma[k]);
					//calcula os valores limites a serem adotados
					MediaLim[k][1] = (Zc/sqrt(contador_dados))*devpad[k]+media[k];
					MediaLim[k][0] = (-Zc/sqrt(contador_dados))*devpad[k]+media[k];
					//mostra os valores..................................
					//cout<<MediaLim[k][0]<<" >= valor "<<media[k]<<" <="<<MediaLim[k][1]<<endl;
				}
			//	MediaLim[k][1] = 2*devpad[k]+media[k];
				//MediaLim[k][0] = (-2*devpad[k])+media[k];
				//cout<<"============================"<<endl<<"============================"<<endl;
				imshow("Video", frame);//mostra o frame

				int a =	cvWaitKey(1);
				if(a == 113){//se apertar 'q' aumenta a regiao a ser medida
					DIM = DIM + unidadeDIM;
					if(DIM>frame.rows)//pra nao aumentar muito
						DIM =frame.rows;
				}else if(a==101){//se apertar 'e' diminui a regiao a ser medida
					DIM = DIM - unidadeDIM;
					if(DIM<unidadeDIM)// nao deixar chegar em zero, senao da ruim na hora de fazer a media 0/0
						DIM=unidadeDIM;
				}else if(a==115){//se apertar 's' desce o quadrado
					yreg += unidadeREG;
					if(yreg>frame.cols/2)
						yreg=frame.cols/2;
				}else if(a==119){//se apertar 'w' sobe o quadrado
					yreg-=unidadeREG;
					if(yreg<(-frame.cols/2))
						yreg=-frame.cols/2;
				}else if(a==97){//se apertar 'a' move o quadrado a direita
					xreg-=unidadeREG;
					if(xreg<(-frame.rows/2))
						xreg=-frame.rows/2;
				}else if(a==100){//se apertar 'd' move o quadrado a esquerda
					xreg+=unidadeREG;
					if(xreg>(frame.rows/2))
						xreg=frame.rows/2;
				}
				else if(a==99){// se apertar 'c' sai da calibracao e salva
					objA.resetAll();
					//salva os valores
					objA.setLimites(MediaLim[0][0],MediaLim[0][1],MediaLim[1][0],MediaLim[1][1],MediaLim[2][0],MediaLim[2][1]);
					//imprime no terminal
					for(int k = 0; k<3; k++){
						cout<<MediaLim[k][0]<<" >= valor "<<media[k]<<" <="<<MediaLim[k][1]<<endl;
					}
					cout<<"==================================================="<<endl;
					break;
				}
			}
		}
	}
	cap.release();
	frame.release();
	hsv.release();

	return 0;
}