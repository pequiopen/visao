#ifndef OBJETO_H_INCLUDED
#define OBJETO_H_INCLUDED
//#include <highgui.h>
#include <opencv2/opencv.hpp>

class Objeto{
	private:
		cv::Point centroid;

		int hueMax;
		int hueMin;
		int satMax;
		int satMin;
		int valMax;
		int valMin;	

		int contador;

		long int quantidadePontos;
		double centroidX;
		double centroidY;
		long double somaX;
		long double somaY;


		Objeto* PonteiroProx;
		Objeto* PonteiroThis;
		Objeto* PonteiroAnte;

	public:
		Objeto();
		void inicializa(int i, int hueA, int hueB, int satA, int satB, int valA, int valB, int b,int r,int g);
		void inicializa(int hueA ,int hueB, int satA, int satB, int valA, int valB,int b,int r,int g, Objeto* pntProp);
		void addPoint(int i, int j);
		void resetObj();
		int B;
		int R;
		int G;
		int id;

		void setPonteiro(Objeto* Anterior, Objeto* Proprio);
		void setPonteiro(Objeto* Proprio);
		void setToUltimo();
		void setLimites(int hueA ,int hueB, int satA, int satB, int valA, int valB);

		Objeto* getAnterior();
		Objeto* getProximo();
		Objeto* getThis();
		Objeto* getUltimo();
		Objeto* getById(int i);
		cv::Point getCentroid();
		long int getQuantidadeP();
		int getContador();

		bool verificaH(int x);
		bool verificaS(int x);
		bool verificaV(int x);

		void resetAll();
		void add();
};

#endif